﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogiqueDeProgrammation
{
    static class OpLogiqueSlide34
    {
        public static bool Menu()
        {
            char[] a = { 'a', 'd', 'i', 'e', 'k' };
            Ensemble A = new Ensemble(new List<char>(a));
            char[] b = { 'i', 'e', 'p', 'm', 'w', 'z' };
            Ensemble B = new Ensemble(new List<char>(b));
            char[] c = { 'k', 'e', 'p', 'm', 'n', 'r' };
            Ensemble C = new Ensemble(new List<char>(c));
            for (int i = 0; i < A.ListeElements.Count; i++)
                Console.Write(A.ListeElements.ElementAt(i) + " ");
            Console.Write("\n");

            for (int i = 0; i < B.ListeElements.Count; i++)
                Console.Write(B.ListeElements.ElementAt(i) + " ");
            Console.Write("\n");

            for (int i = 0; i < C.ListeElements.Count; i++)
                Console.Write(C.ListeElements.ElementAt(i) + " ");
            Console.Write("\n");

            Ensemble D = Ensemble.ET(A, B);
            for (int i = 0; i < D.ListeElements.Count; i++)
                Console.Write(D.ListeElements.ElementAt(i) + " ");
            Console.Write("\n");

            Ensemble E = Ensemble.OU(A, C);
            for (int i = 0; i < E.ListeElements.Count; i++)
                Console.Write(E.ListeElements.ElementAt(i) + " ");
            Console.Write("\n");

            Ensemble F = Ensemble.XOR(B, C);
            for (int i = 0; i < F.ListeElements.Count; i++)
                Console.Write(F.ListeElements.ElementAt(i) + " ");
            Console.Write("\n");

            return Fonctions.ReponseBool("Voulez-vous exécuter un autre programme ? (o ou n)");
        }
        static int ExercicePage34()
        {
            char[] a = { 'a', 'd', 'i', 'e', 'k' };
            Ensemble A = new Ensemble(new List<char>(a));
            char[] b = { 'i', 'e', 'p', 'm', 'w', 'z' };
            Ensemble B = new Ensemble(new List<char>(b));
            char[] c = { 'k', 'e', 'p', 'm', 'n', 'r' };
            Ensemble C = new Ensemble(new List<char>(c));
            for (int i = 0; i < A.ListeElements.Count; i++)
                Console.Write(A.ListeElements.ElementAt(i) + " ");
            Console.Write("\n");

            for (int i = 0; i < B.ListeElements.Count; i++)
                Console.Write(B.ListeElements.ElementAt(i) + " ");
            Console.Write("\n");

            for (int i = 0; i < C.ListeElements.Count; i++)
                Console.Write(C.ListeElements.ElementAt(i) + " ");
            Console.Write("\n");

            Ensemble D = Ensemble.ET(A, B);
            for (int i = 0; i < D.ListeElements.Count; i++)
                Console.Write(D.ListeElements.ElementAt(i) + " ");
            Console.Write("\n");

            Ensemble E = Ensemble.OU(A, C);
            for (int i = 0; i < E.ListeElements.Count; i++)
                Console.Write(E.ListeElements.ElementAt(i) + " ");
            Console.Write("\n");

            Ensemble F = Ensemble.XOR(B, C);
            for (int i = 0; i < F.ListeElements.Count; i++)
                Console.Write(F.ListeElements.ElementAt(i) + " ");
            Console.Write("\n");

            return 0;
        }
        static void MensNight()
        {
            bool homme = Fonctions.ReponseBool("Etes-vous un homme (o) ou une femme (n) ?");
            bool majeur = Fonctions.ReponseBool("Avez-vous plus de 18 ans, 18 compris (o) ou moins de 18 ans (n) ?");
            if (homme && majeur)
                Console.WriteLine("Vous pouvez participer à la Men's Night.");
            else
                Console.WriteLine("Vous ne pouvez pas participer à la Men's Night.");
        }
        static void Manteau()
        {
            bool tempMoinsDe18 = !Fonctions.ReponseBool("Fait-il plus de 18 degrés, 18 compris (o) ou moins de 18 degrés (n) ?");
            bool pluie = Fonctions.ReponseBool("Pleut-il (o) ou (n) ?");
            if (tempMoinsDe18 && pluie)
                Console.WriteLine("Vous devez prendre votre manteau.");
            else
                Console.WriteLine("Vous ne devez pas prendre votre manteau.");
        }
        static void AlbumTintin()
        {
            int age = Fonctions.ReponseEntiere("Quel âge avez-vous ?");
            if ((age < 7) || (age > 74))
                Console.WriteLine("Vous ne pouvez pas recevoir un album de Tintin.");
            else
                Console.WriteLine("Vous pouvez recevoir un album de Tintin.");
        }
        static void PorteClesTintin()
        {
            int age = Fonctions.ReponseEntiere("Quel âge avez-vous ?");
            if (((age >=3) && (age < 7)) || (age > 74))
                Console.WriteLine("Vous pouvez recevoir un porte-clés Tintin.");
            else
                Console.WriteLine("Vous ne pouvez pas recevoir un porte-clés Tintin.");
        }
    }
    class Ensemble
    {
        public List<Char> ListeElements { get; set; }

        public Ensemble(List<Char> listeElements)
        {
            ListeElements = listeElements;
        }

        public static Ensemble ET(Ensemble a, Ensemble b)
        {
            List<Char> listeET = new List<Char>();
            for (int i = 0; i < b.ListeElements.Count; i++)
                if (a.ListeElements.Contains(b.ListeElements.ElementAt(i)))
                    listeET.Add(b.ListeElements.ElementAt(i));
            return new Ensemble(listeET);
        }
        public static Ensemble OU(Ensemble a, Ensemble b)
        {
            List<Char> listeOU = new List<Char>(a.ListeElements);
            for (int i = 0; i < b.ListeElements.Count; i++)
                if (!listeOU.Contains(b.ListeElements.ElementAt(i)))
                    listeOU.Add(b.ListeElements.ElementAt(i));
            return new Ensemble(listeOU);
        }
        public static Ensemble XOR(Ensemble a, Ensemble b)
        {
            List<Char> listeXOR = new List<Char>();
            for (int i = 0; i < b.ListeElements.Count; i++)
            {
                if (!b.ListeElements.Contains(a.ListeElements.ElementAt(i)))
                    listeXOR.Add(a.ListeElements.ElementAt(i));
                if (!a.ListeElements.Contains(b.ListeElements.ElementAt(i)))
                    listeXOR.Add(b.ListeElements.ElementAt(i));
            }
            return new Ensemble(listeXOR);
        }
        private static Ensemble NON()
        {
            return new Ensemble(null);
        }
    }
}
