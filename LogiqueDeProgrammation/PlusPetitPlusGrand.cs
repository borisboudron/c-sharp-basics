﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogiqueDeProgrammation
{
    static class PlusPetitPlusGrand
    {
        public static bool Jeu()
        {
            const int LIMITE_INFERIEURE = 0;
            const int LIMITE_SUPERIEURE = 100000;
            const int ESSAIS_MAX = 20;
            bool fin = false;
            Random rnd = new Random();
            int nombreAleatoire = rnd.Next(LIMITE_INFERIEURE, LIMITE_SUPERIEURE);
            int essais = 1;
            string plurielRestants = "s";
            string plurielEssais = "";
            Console.WriteLine("Vous avez {0} essais pour trouver le nombre caché.", ESSAIS_MAX);
            while ((!fin) && (essais <= ESSAIS_MAX))
            {
                int nombre = Fonctions.ReponseEntiere("Entrez un nombre entre " + LIMITE_INFERIEURE + " et " + LIMITE_SUPERIEURE + " : ");
                if ((nombre < 0) || (nombre > 100000))
                    Console.WriteLine("Hors limites. Il vous reste {0} essai{1}.", ESSAIS_MAX - essais, plurielRestants);
                else if (nombreAleatoire < nombre)
                {
                    Console.WriteLine("Plus petit. Il vous reste {0} essai{1}.", ESSAIS_MAX - essais, plurielRestants);
                    essais++;
                }
                else if (nombreAleatoire > nombre)
                {
                    Console.WriteLine("Plus grand. Il vous reste {0} essai{1}.", ESSAIS_MAX - essais, plurielRestants);
                    essais++;
                }
                else
                {
                    fin = true;
                    Console.WriteLine("Gagné ! Vous avez trouvé en {0} essai{1}.", essais, plurielEssais);
                }
                plurielEssais = "s";
                if (essais == ESSAIS_MAX - 1)
                    plurielRestants = "";
            }
            if (fin)
                Console.WriteLine("Bravo !");
            else
                Console.WriteLine("Vous avez perdu. Désolé. La réponse était {0} !", nombreAleatoire);
            return Fonctions.ReponseBool("Voulez-vous exécuter un autre programme ? (o ou n)");
        }
    }
}
