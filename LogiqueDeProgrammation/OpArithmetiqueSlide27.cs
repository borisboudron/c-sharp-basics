﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogiqueDeProgrammation
{
    static class OpArithmetiqueSlide27
    {
        public static bool Program()
        {
            bool choixValable = false;
            while (!choixValable)
            {
                int reponse = Fonctions.ReponseEntiere("Voulez-vous l'exercice de la page 27 ou 28 ?");
                if (reponse == 27)
                {
                    Program1();
                    choixValable = true;
                }
                else if (reponse == 28)
                {
                    Program2();
                    choixValable = true;
                }
                else Console.WriteLine("Choix non valable. Recommencez.");
            }
            return Fonctions.ReponseBool("Voulez-vous exécuter un autre programme ? (o ou n)");
        }
        public static int Program1()
        {
            int a = Fonctions.ReponseEntiere("Donnez 1 valeur pour a.");
            int b = Fonctions.ReponseEntiere("Donnez 1 valeur pour b.");
            Console.WriteLine("Vous avez introduit les valeurs {0} et {1}", a, b);
            Console.WriteLine("c <- a + b");
            int c = a + b;
            Console.WriteLine("b <- a + b");
            b = a + b;
            Console.WriteLine("a <- c");
            a = c;
            Console.WriteLine("Les valeurs pour a, b et c sont {0}, {1} et {2}", a, b, c);
            return 0;
        }
        public static int Program2()
        {
            int a = Fonctions.ReponseEntiere("Donnez 1 valeur pour a.");
            int b = Fonctions.ReponseEntiere("Donnez 1 valeur pour b.");
            Console.WriteLine("Vous avez introduit les valeurs {0} et {1}", a, b);
            Console.WriteLine("c <- a % b");
            int c = a % b;
            Console.WriteLine("b <- a Div b");
            b = a / b;
            Console.WriteLine("Les valeurs pour a, b et c sont {0}, {1} et {2}", a, b, c);
            return 0;
        }
    }
}
