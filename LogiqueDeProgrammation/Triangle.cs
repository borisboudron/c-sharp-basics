﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogiqueDeProgrammation
{
    static class Triangle
    {
        public static bool Menu()
        {
            int longueur = Fonctions.ReponseEntiere("Quelle est la longueur du côté du triangle souhaité ?");
            bool plein = Fonctions.ReponseBool("Voulez-vous un triangle plein (o) ou creux (n) ?");
            Console.WriteLine("Où voulez-vous l'angle droit ?");
            Console.WriteLine("1 : en haut à gauche.");
            Console.WriteLine("2 : en haut à droite.");
            Console.WriteLine("3 : en bas à gauche.");
            int choixAngle = Fonctions.ReponseEntiere("4 : en bas à droite.");
            DessineTriangle(longueur, plein, choixAngle);
            return Fonctions.ReponseBool("Voulez-vous exécuter un autre programme ? (o ou n)");
        }
        public static int DessineTriangle(int l, bool plein, int choixInterieur)
        {
            string sommetGauche = "*";
            string sommetDroit = Fonctions.RepeteCar(' ', l - 1) + '*';
            string grandCote = Fonctions.RepeteCar('*', l);
            string chaine = "";
            if (plein && (choixInterieur == 1))
            // Triangle plein haut gauche
            {
                // Pour chaque ligne
                for (int i = 0; i < l; i++)
                {
                    // Construction
                    chaine = Fonctions.RepeteCar('*', l - i);
                    // Affichage
                    Console.WriteLine(chaine);
                }
            }
            else if (!plein && (choixInterieur == 1))
            // Triangle creux haut gauche
            {
                // Ligne supérieure
                Console.WriteLine(grandCote);
                // Pour chaque ligne intérieure
                for (int i = 1; i < l - 1; i++)
                {
                    // Construction
                    chaine = "*";
                    chaine += Fonctions.RepeteCar(' ', l - i - 2);
                    chaine += '*';
                    // Affichage
                    Console.WriteLine(chaine);
                }
                // Ligne inférieure
                Console.WriteLine(sommetGauche);
            }
            else if (plein && (choixInterieur == 2))
            // Triangle plein haut droit
            {
                // Pour chaque ligne
                for (int i = 0; i < l; i++)
                {
                    // Construction
                    chaine = Fonctions.RepeteCar(' ', i);
                    chaine += Fonctions.RepeteCar('*', l - i);
                    // Affichage
                    Console.WriteLine(chaine);
                }
            }
            else if (!plein && (choixInterieur == 2))
            // Triangle creux haut droit
            {
                // Ligne supérieure
                Console.WriteLine(grandCote);
                // Pour chaque ligne
                for (int i = 1; i < l - 1; i++)
                {
                    // Construction
                    chaine = Fonctions.RepeteCar(' ', i);
                    chaine += '*';
                    chaine += Fonctions.RepeteCar(' ', l - i - 2);
                    chaine += '*';
                    // Affichage
                    Console.WriteLine(chaine);
                }
                // Ligne inférieure
                Console.WriteLine(sommetDroit);
            }
            else if (plein && (choixInterieur == 3))
            // Triangle plein bas gauche
            {
                // Pour chaque ligne
                for (int i = 0; i < l; i++)
                {
                    // Construction
                    chaine = Fonctions.RepeteCar('*', i + 1);
                    // Affichage
                    Console.WriteLine(chaine);
                }
            }
            else if (!plein && (choixInterieur == 3))
            // Triangle creux bas gauche
            {
                // Ligne supérieure
                Console.WriteLine(sommetGauche);
                // Pour chaque ligne
                for (int i = 1; i < l - 1; i++)
                {
                    // Construction
                    chaine = "*";
                    chaine += Fonctions.RepeteCar(' ', i - 1);
                    chaine += '*';
                    // Affichage
                    Console.WriteLine(chaine);
                }
                // Ligne inférieure
                Console.WriteLine(grandCote);
            }
            else if (plein && (choixInterieur == 4))
            // Triangle plein bas droit
            {
                // Pour chaque ligne
                for (int i = 0; i < l; i++)
                {
                    // Construction
                    chaine = Fonctions.RepeteCar(' ', l - i - 1);
                    chaine += Fonctions.RepeteCar('*', i + 1);
                    // Affichage
                    Console.WriteLine(chaine);
                }
            }
            else if (!plein && (choixInterieur == 4))
            // Triangle creux bas droit
            {
                // Ligne supérieure
                Console.WriteLine(sommetDroit);
                // Pour chaque ligne
                for (int i = 1; i < l - 1; i++)
                {
                    // Construction
                    chaine = Fonctions.RepeteCar(' ', l - i - 1);
                    chaine += '*';
                    chaine += Fonctions.RepeteCar(' ', i - 1);
                    chaine += '*';
                    // Affichage
                    Console.WriteLine(chaine);
                }
                // Ligne inférieure
                Console.WriteLine(grandCote);
            }
            return 0;
        }
    }
}
