﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogiqueDeProgrammation
{
    static class Octogone
    {
        public static bool Menu()
        {
            int longueur = Fonctions.ReponseEntiere("Quelle est la longueur de l'octogone souhaité ?");
            bool plein = Fonctions.ReponseBool("Voulez-vous un octogone plein (o) ou creux (n) ?");
            DessineOctogone(longueur, plein);
            return Fonctions.ReponseBool("Voulez-vous exécuter un autre programme ? (o ou n)");
        }
        public static int DessineOctogone(int l, bool plein)
        {
            string chaineExtremite = Fonctions.RepeteCar(' ', l - 1) + Fonctions.RepeteCar('*', l) + Fonctions.RepeteCar(' ', l - 1);
            // Ligne supérieure
            Console.WriteLine(chaineExtremite);
            if (plein)
            // Octogone plein
            {
                string chaineMilieu = Fonctions.RepeteCar('*', 3 * l - 2);
                string chaine = "";
                for (int i = 1; i < l - 1; i++)
                // Diagonales supérieures
                {
                    chaine = Fonctions.RepeteCar(' ', l - 1 - i);
                    chaine += Fonctions.RepeteCar('*', l + 2 * i);
                    Console.WriteLine(chaine);
                }
                // Milieu de l'octogone
                for (int i = l - 1; i < 2 * l - 1; i++)
                    Console.WriteLine(chaineMilieu);
                for (int i = 2 * l - 1; i < 3 * l - 3; i++)
                // Diagonales inférieures
                {
                    chaine = Fonctions.RepeteCar(' ', l - 1 - (3 * l - 3 - i));
                    chaine += Fonctions.RepeteCar('*', l + 2 * (3 * l - 3 - i));
                    Console.WriteLine(chaine);
                }
            }
            else
            // Octogone creux
            {
                string chaineMilieu = "*" + Fonctions.RepeteCar(' ', 3 * l - 4) + "*";
                /* Méthode du quadrillage à diagonales séparées

                for (int i = 1; i <= 3 * l - 2; i++)
                {
                    int j;
                    chaine = "";
                    if ((i == 1) || (i == 3 * l - 2))
                    {
                        for (j = 1; j <= l - 1; j++)
                            chaine += " ";
                        for (j = l; j <= 2 * l - 1; j++)
                            chaine += "*";
                    }
                    else if ((i >= l) && (i <= 2 * l - 1))
                    {
                        chaine = "*";
                        for (j = 2; j <= 3 * l - 3; j++)
                            chaine += " ";
                        chaine += "*";
                    }
                    else if (i < (3 * l - 2) / 2)
                    {
                        for (j = 1; j <= l - i; j++)
                            chaine += " ";
                        chaine += "*";
                        for (j = l - i + 2; j <= 2 * l - 1 + i - 2; j++)
                            chaine += " ";
                        chaine += "*";
                    }
                    else
                    {
                        for (j = 1; j <= l - (3 * l - 1 - i); j++)
                            chaine += " ";
                        chaine += "*";
                        for (j = l - (3 * l - 1 - i) + 2; j <= (2 * l - 1) + (3 * l - 1 - i) - 2; j++)
                            chaine += " ";
                        chaine += "*";
                    }
                    Console.WriteLine(chaine);
                }*/
                /* Méthode du quadrillage à diagonales regroupées

                for (int i = 1; i <= 3 * l - 2; i++)
                {
                    int j;
                    chaine = "";
                    if ((i == 1) || (i == 3 * l - 2))
                    {
                        for (j = 1; j <= l - 1; j++)
                            chaine += " ";
                        for (j = l; j <= 2 * l - 1; j++)
                            chaine += "*";
                    }
                    else if ((i >= l) && (i <= 2 * l - 1))
                    {
                        chaine = "*";
                        for (j = 2; j <= 3 * l - 3; j++)
                            chaine += " ";
                        chaine += "*";
                    }
                    else
                    {
                        for (j = 1; j <= (System.Math.Abs(2 * i - (3 * l - 1)) - (l - 1)) / 2; j++)
                            chaine += " ";
                        chaine += "*";
                        for (j = (System.Math.Abs(2 * i - (3 * l - 1)) - (l - 5)) / 2; j <= ((7 * l - 7) - System.Math.Abs((3 * l - 1) - 2 * i)) / 2; j++)
                            chaine += " ";
                        chaine += "*";
                    }
                    Console.WriteLine(chaine);
                }*/
                /* Méthode des distances à diagonales séparées */

                string chaine = "";
                for (int i = 1; i < l - 1; i++)
                // Diagonales supérieures
                {
                    chaine = Fonctions.RepeteCar(' ', l - 1 - i);
                    chaine += "*";
                    chaine += Fonctions.RepeteCar(' ', l + 2 * (i - 1));
                    chaine += "*";
                    Console.WriteLine(chaine);
                }
                for (int i = l - 1; i < 2 * l - 1; i++)
                    // Milieu de l'octogone
                    Console.WriteLine(chaineMilieu);
                for (int i = 2 * l - 1; i < 3 * l - 3; i++)
                // Diagonales inférieures
                {
                    chaine = Fonctions.RepeteCar(' ', l - 1 - (3 * l - 3 - i));
                    chaine += "*";
                    chaine += Fonctions.RepeteCar(' ', l + 2 * ((3 * l - 3 - i) - 1));
                    chaine += "*";
                    Console.WriteLine(chaine);
                }
                /* Méthode des distances à diagonales regroupées

                for (int i = 1; i <= 3 * l - 2; i++)
                {
                    int j;
                    chaine = "";
                    if ((i == 1) || (i == 3 * l - 2))
                    {
                        for (j = 1; j <= l - 1; j++)
                            chaine += " ";
                        for (j = 1; j <= l; j++)
                            chaine += "*";
                    }
                    else if ((i >= l) && (i <= 2 * l - 1))
                    {
                        chaine = "*";
                        for (j = 1; j <= 3 * l - 4; j++)
                            chaine += " ";
                        chaine += "*";
                    }
                    else
                    {
                        for (j = 1; j <= (System.Math.Abs(2 * i - (3 * l - 1)) - (l - 1)) / 2; j++)
                            chaine += " ";
                        chaine += "*";
                        for (j = 1; j <= -System.Math.Abs(2 * i - (3 * l - 1)) + l + System.Math.Abs(5 - 3 * l); j++)
                            chaine += " ";
                        chaine += "*";
                    }
                    Console.WriteLine(chaine);
                }
                */
            }
            // Ligne inférieure
            Console.WriteLine(chaineExtremite);
            return 0;
        }
    }
}
