﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogiqueDeProgrammation
{
    static class Puissance4
    {
        public static bool Program()
        {
            bool vsCOM = Fonctions.ReponseBool("Un joueur (o) ou 2 joueurs (n) ?");
            const int L = 6;
            const int C = 7;
            char[,] t = new char[L,C];
            InitialiserTableau(t, L, C);
            AfficherTableau(t, L, C);
            char joueur = 'J';
            bool vainqueur = false;
            int coups = 0;
            while ((!vainqueur)&&(coups < 42))
            {
                int colonne = 0;
                int ligne = 0;
                // Placement du pion
                bool bonPlacement = false;
                do
                {
                    string phrase = "Joueur " + joueur + ", jouez. Saisissez la colonne où vous désirez jouer (entre 1 et 7).";
                    if ((vsCOM)&&(joueur != 'J'))
                        colonne = IA1CoupDAvance(t, L, C, joueur);
                    else
                        colonne = Fonctions.ReponseEntiere(phrase) - 1;
                    if ((colonne < 0) || (colonne > 6))
                        Console.WriteLine("Mauvaise colonne.");
                    else
                    {
                        ligne = PlacerPion(t, L, colonne, joueur, true);
                        if (ligne == 6)
                            Console.WriteLine("Colonne pleine.");
                        else
                            bonPlacement = true;
                    }
                } while (!bonPlacement);
                // Affichage
                AfficherTableau(t, L, C);
                // Recherche d'un vainqueur
                if (vainqueur = ChercherVainqueur(t, L, C, ligne, colonne))
                    Console.WriteLine("Vainqueur : joueur {0}", joueur);
                else if (joueur == 'J')
                {
                    joueur = 'R';
                    coups++;
                }
                else
                {
                    joueur = 'J';
                    coups++;
                }
            }
            if (!vainqueur)
                Console.WriteLine("Aucun gagnant");
            return Fonctions.ReponseBool("Voulez-vous exécuter un autre programme ? (o ou n)");
        }
        public static void InitialiserTableau(char[,] t, int l, int c)
        {
            for (int i = 0; i < l; i++)
                for (int j = 0; j < c; j++)
                    t[i,j] = ' ';
        }
        public static void AfficherTableau(char[,] t, int L, int C)
        {
            Console.WriteLine("-----------------------------");
            Console.WriteLine("| 1 | 2 | 3 | 4 | 5 | 6 | 7 |");
            Console.WriteLine("|---------------------------|");
            for (int i = 0; i < L; i++)
            {
                string ligne = "| ";
                for (int j = 0; j < C; j++)
                {
                    ligne += t[L - 1 - i, j] + " |";
                    if (j != C - 1)
                        ligne += " ";
                }
                Console.WriteLine(ligne);
                Console.WriteLine("|---------------------------|");
            }
            Console.WriteLine("");
        }
        public static int PlacerPion(char[,] t, int L, int choixColonne, char car, bool placement)
        {
            int l = 0;
            bool place = false;
            while ((!place) && (l < L))
            {
                if (t[l, choixColonne] == ' ')
                {
                    if (placement)
                        t[l, choixColonne] = car;
                    place = true;
                }
                else
                    l++;
            }
            return l;
        }
        public static bool ChercherVainqueur(char[,] t, int L, int C, int l, int c)
        {
            bool trouve = false;
            int i = 0;
            int alignes = 0;
            // Se balader le long de la ligne
            while ((i < C) && (!trouve))
            {
                if (t[l, i] == t[l, c])
                    alignes++;
                else
                    alignes = 0;
                trouve = (alignes == 4);
                i++;
            }
            // Se balader le long de la colonne
            if (l > 2)
            {
                i = 0;
                alignes = 0;
                while ((i < l + 1) && (!trouve))
                {
                    if (t[i, c] == t[l, c])
                        alignes++;
                    else
                        alignes = 0;
                    trouve = (alignes == 4);
                    i++;
                }
            }
            // Se balader le long de la diagonale ascendante
            int minInf = -Math.Min(l, c);
            int minSup = Math.Min(L - l, C - c);
            i = minInf;
            alignes = 0;
            if (minSup - minInf >= 4)
            {
                while ((i < minSup) && (!trouve))
                {
                    if (t[l + i, c + i] == t[l, c])
                        alignes++;
                    else
                        alignes = 0;
                    trouve = (alignes == 4);
                    i++;
                }
            }
            // Se balader le long de la diagonale descendante
            minInf = -Math.Min((L - 1) - l, c);
            minSup = Math.Min(l + 1, C - c);
            i = minInf;
            alignes = 0;
            if (minSup - minInf >= 4)
            {
                while ((i < minSup) && (!trouve))
                {
                    if (t[l - i, c + i] == t[l, c])
                        alignes++;
                    else
                        alignes = 0;
                    trouve = (alignes == 4);
                    i++;
                }
            }
            return trouve;
        }
        public static bool PositionValide(int L, int C, int l, int c)
        {
            bool reponse = true;
            if ((l < 0) || (l >= L) || (c < 0) || (c >= C))
                reponse = false;
            return reponse;
        }
        public static int IA1CoupDAvance(char[,] t, int L, int C, char joueur)
        {
            Random r = new Random();
            int colonne;
            char joueurAdverse;
            if (joueur == 'J')
                joueurAdverse = 'R';
            else
                joueurAdverse = 'J';
            if ((colonne = ChercherColonneGagnante(t, L, C, joueur)) < 7)
                DoNothing();
            else if ((colonne = ChercherColonneGagnante(t, L, C, joueurAdverse)) < 7)
                DoNothing();
            else
                colonne = r.Next(0, 6);
            return colonne;
        }
        public static int ChercherColonneGagnante(char[,] t, int L, int C, char joueur)
        {
            bool vainqueur = false;
            int c = 0;
            while ((!vainqueur) && (c < C))
            {
                int ligne = PlacerPion(t, L, c, joueur, true);
                if ((ligne < L) && (ChercherVainqueur(t, L, C, ligne, c)))
                {
                    t[ligne, c] = ' ';
                    vainqueur = true;
                }
                else if (ligne < L)
                {
                    t[ligne, c] = ' ';
                    c++;
                }
                else
                    c++;
            }
            return c;
        }
        public static void DoNothing()
        {

        }
    }
}
