﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogiqueDeProgrammation
{
    static class ExercicesTableaux
    {
        public static bool Program()
        {
            bool choixValable = false;
            while (!choixValable)
            {
                int reponse = Fonctions.ReponseEntiere("Quel exercice ?");
                if (reponse == 1)
                {
                    Exercice1();
                    choixValable = true;
                }
                else if (reponse == 2)
                {
                    Exercice2();
                    choixValable = true;
                }
                else if (reponse == 3)
                {
                    //Exercice3();
                    choixValable = true;
                }
                else if (reponse == 4)
                {
                    //Exercice4();
                    choixValable = true;
                }
                else Console.WriteLine("Choix non valable. Recommencez.");
            }
            return Fonctions.ReponseBool("Voulez-vous exécuter un autre programme ? (o ou n)");
        }
        public static bool Exercice1()
        {
            Console.WriteLine("Entrez une phrase.");
            string phrase = Console.ReadLine();
            int l = phrase.Length;
            char[] tableau = new char[l];
            int i;
            for (i = 0; i < l; i++)
                tableau[i] = phrase[i];

            Console.WriteLine("Entrez un seul caractère.");
            char c = Console.ReadLine()[0];
            i = 0;
            while ((i < l) && (c != phrase[i]))
                i++;
            if (i == l)
                Console.WriteLine("Le caractère {0} n'est pas dans \"{1}\".", c, phrase);
            else
                Console.WriteLine("Le caractère {0} est en position {1}.", c, i + 1);

            int nbC = 0;
            for (i = 0; i < l; i++)
                if (c == phrase[i])
                    nbC++;
            Console.WriteLine("Le caractère {0} apparaît {1} fois.", c, nbC);

            char[] inverse = new char[l];
            Console.Write("\nLa phrase inverse de \"{0}\" est \"", phrase);
            for (i = 0; i < l; i++)
            {
                inverse[i] = phrase[l - i - 1];
                Console.Write("{0}", inverse[i]);
            }
            Console.WriteLine("\".\n");

            return Fonctions.ReponseBool("Voulez-vous exécuter un autre programme ? (o ou n)");
        }
        public static bool Exercice2()
        {
            const int X = 5;
            const int Y = 5;
            int valeur = Fonctions.ReponseEntiere("Entrez une valeur.");
            int[,] tableau = new int[X,Y];
            for (int i = 0; i < X; i++)
                for (int j = 0; j < Y; j++)
                    tableau[i,j] = valeur++ % 14;
            AfficherTableau(tableau, X, Y);

            AfficherTableau(SommeMatrice(tableau, tableau, X, Y), X, Y);

            AfficherTableau(TriTableau(tableau, X, Y), X * Y);

            return Fonctions.ReponseBool("Voulez-vous exécuter un autre programme ? (o ou n)");
        }
        public static int AfficherTableau(int[] tableau, int x)
        {
            string affichage = "Tableau :\n";
            for (int i = 0; i < x; i++)
            {
                affichage += tableau[i];
                if (i < x - 1)
                    affichage += " ";
            }
            affichage += "\n";
            Console.WriteLine(affichage);
            return 0;
        }
        public static int AfficherTableau(int[,] tableau, int x, int y)
        {
            string affichage = "Tableau :\n";
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    affichage += tableau[i, j];
                    if (j < y - 1)
                        affichage += " ";
                }
                if (i < x - 1)
                    affichage += "\n";
            }
            Console.WriteLine(affichage);
            return 0;
        }
        public static int[,] SommeMatrice(int[,] t1, int[,] t2, int x, int y)
        {
            int[,] resultat = new int[x, y];
            for (int i = 0; i < x; i++)
                for (int j = 0; j < y; j++)
                    resultat[i, j] = t1[i, j] + t2[i, j];
            return resultat;
        }
        public static int[] TriTableau(int[,] tableau, int x, int y)
        {
            int[] resultat = new int[x * y];
            for (int i = 0; i < x; i++)
                for (int j = 0; j < y; j++)
                {
                    if ((i == 0) && (j == 0))
                        resultat[y * i + j] = tableau[i, j];
                    else
                    {
                        int position = TrouverPosition(resultat, y * i + j, tableau[i, j]);
                        DecalerElementsApres(resultat, x * y, position);
                        resultat[position] = tableau[i, j];
                    }
                }
            return resultat;
        }
        public static int TrouverPosition(int[] tableau, int tailleRemplie, int nombreAClasser)
        {
            int position = 0;
            bool positionTrouvee = false;
            while ((!positionTrouvee) && (position < tailleRemplie))
            {
                if (nombreAClasser > tableau[position])
                    position++;
                else
                    positionTrouvee = true;
            }
            return position;
        }
        public static int DecalerElementsApres(int[] tableau, int taille, int position)
        {
            for (int i = taille - 1; i > position; i--)
                tableau[i] = tableau[i - 1];
            return 0;
        }
    }
}
