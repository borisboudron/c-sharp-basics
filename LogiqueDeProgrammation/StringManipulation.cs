﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogiqueDeProgrammation
{
    static class StringManipulation
    {
        public static bool Program()
        {
            // AFFICHER "Entrez une chaîne de caractères :"
            Console.WriteLine("Entrez une chaîne de caractères à manipuler :");
            // SAISIR chaîne
            string chaine = Console.ReadLine();
            // AFFICHER "Entrez une sous-chaîne de caractères ou un caractère simple :"
            Console.WriteLine("Entrez une sous-chaîne de caractères ou un caractère simple :");
            // SAISIR car
            string sousChaine = Console.ReadLine();
            char car = sousChaine[0];
            if (sousChaine.Length == 1)
                car = sousChaine[0];
            // AFFICHER "Entrez une sous-chaîne de caractères de remplacement :"
            Console.WriteLine("Entrez une sous-chaîne de caractères de remplacement :");
            // SAISIR car
            string sousChaineRemplacement = Console.ReadLine();
            // TROUVER LA 1ERE POSITION DU CARACTERE
            if (sousChaine.Length == 1)
            {
                int position = Caractere1erePosition(chaine, car);
                if (position != chaine.Length)
                    Console.WriteLine("La 1ère occurence du caractère {0} est en position {1}", car, position + 1);
                else
                    Console.WriteLine("Aucune occurence du caractère {0} dans la chaîne {1}", car, chaine);
            }
            else
            {
                int position = SousChaine1erePosition(chaine, sousChaine);
                if (position != chaine.Length)
                    Console.WriteLine("La 1ère occurence de la sous-chaîne \"{0}\" est en position {1}", sousChaine, position + 1);
                else
                    Console.WriteLine("Aucune occurence de la sous-chaîne \"{0}\" dans la chaîne {1}", sousChaine, chaine);
            }
            // COMPTER LE NOMBRE D'OCCURENCES DU CARACTERE
            if (sousChaine.Length == 1)
            {
                int nbCar = CaractereOccurences(chaine, car);
                Console.WriteLine("Le nombre d'occurences du caractère {0} est de {1}", car, nbCar);
            }
            else
            {
                int nbSC = SousChaineOccurences(chaine, sousChaine);
                Console.WriteLine("Le nombre d'occurences de la sous-chaîne \"{0}\" est de {1}", sousChaine, nbSC);
            }
            // AFFICHER L'INVERSE DE LA CHAINE DE CARACTERES
            string inverse = ChaineInverse(chaine);
            Console.WriteLine("L'inverse de \"{0}\" est \"{1}\"", chaine, inverse);
            // REMPLACER LA SOUS-CHAINE PAR UNE AUTRE SOUS CHAINE
            string chaineModifiee = RemplacementSousChaine(chaine, sousChaine, sousChaineRemplacement);
            Console.WriteLine("En remplaçant \"{0}\" par \"{1}\" dans \"{2}\", on obtient \"{3}\"", sousChaine, sousChaineRemplacement, chaine, chaineModifiee);
            return Fonctions.ReponseBool("Voulez-vous exécuter un autre programme ? (o ou n)");
        }
        public static int Caractere1erePosition(string contenant, char aTrouver)
        {
            int position = 0;
            while ((position < contenant.Length) && (aTrouver != contenant[position]))
                position++;
            return position;
        }
        public static int SousChaine1erePosition(string contenant, string aTrouver)
        {
            int position = 0;
            while ((position < contenant.Length - aTrouver.Length + 1) && (aTrouver != contenant.Substring(position, aTrouver.Length)))
                position++;
            return position;
        }
        public static int CaractereOccurences(string contenant, char aTrouver)
        {
            int occurences = 0;
            for (int i = 0; i < contenant.Length; i++)
                if (aTrouver == contenant[i])
                    occurences++;
            return occurences;
        }
        public static int SousChaineOccurences(string contenant, string aTrouver)
        {
            int occurences = 0;
            for (int i = 0; i < contenant.Length - aTrouver.Length + 1; i++)
                if (aTrouver == contenant.Substring(i, aTrouver.Length))
                    occurences++;
            return occurences;
        }
        public static string ChaineInverse(string chaine)
        {
            string inverse = "";
            for (int i = chaine.Length - 1; i > -1; i--)
                inverse += chaine[i];
            return inverse;
        }
        public static string RemplacementSousChaine(string contenant, string aRemplacer, string remplacement)
        // on reçoit en paramètre 3 strings : la chaine à analyser, la chaine à remplacer et la chaîne par laquelle on remplace l'autre
        // Variables :  nouvelleChaine                                      : string
        //              positionSousChaineTrouvee, positionBase             : int
        {
            // Initialisation des variables
            string nouvelleChaine = "";
            int positionSousChaineTrouvee = 0;
            int positionBase = 0;
            // POUR positionBase < strlen(contenant) - strlen(aRemplacer) + 1
            while (positionBase < contenant.Length - aRemplacer.Length + 1)
            {
                // positionSousChaineTrouvee = SousChaine1erePosition(substring(contenant, positionBase), aRemplacer)
                positionSousChaineTrouvee = SousChaine1erePosition(contenant.Substring(positionBase), aRemplacer);
                // nouvelleChaine = nouvelleChaine & Substring(contenant, positionBase, positionSousChaineTrouvee)
                nouvelleChaine += contenant.Substring(positionBase, positionSousChaineTrouvee);
                // SI positionBase + positionSousChaineTrouvee < strlen(contenant) - strlen(aRemplacer) + 1
                if (positionBase + positionSousChaineTrouvee < contenant.Length - aRemplacer.Length + 1)
                {
                    // nouvelleChaine = nouvelleChaine & remplacement
                    nouvelleChaine += remplacement;
                    // positionBase += positionSousChaineTrouvee + aRemplacer.Length
                    positionBase += positionSousChaineTrouvee + aRemplacer.Length;
                }
                // SINON
                else
                    // positionBase += positionSousChaineTrouvee
                    positionBase += positionSousChaineTrouvee;
                // FINSI
            // FINPOUR
            }
            // nouvelleChaine = nouvelleChaine & contenant.Substring(positionBase);
            nouvelleChaine += contenant.Substring(positionBase);
            // RETOURNE nouvelleChaine
            return nouvelleChaine;
        }
    }
}
