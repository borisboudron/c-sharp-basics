﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogiqueDeProgrammation
{
    static class PerimetreAireCercle
    {
        public static bool Menu()
        {
            bool fin = false;
            while (!fin)
            {
                int r = Fonctions.ReponseEntiere("Entrez un entier positif :");
                if (r > 0)
                {
                    Console.WriteLine("Le périmètre du cercle de rayon {0} vaut 2 * Pi * {0} =  {1}", r, PerimetreCercle(r));
                    Console.WriteLine("L'aire du cercle de rayon {0} vaut Pi * {0}² =  {1}", r, AireCercle(r));
                    fin = true;
                }
                else
                    Console.WriteLine("Ce n'est pas un entier positif. Réessayez.");
            }
            return Fonctions.ReponseBool("Voulez-vous exécuter un autre programme ? (o ou n)");
        }
        static double PerimetreCercle(int r)
        {
            return 2 * Math.PI * r;
        }
        static double AireCercle(int r)
        {
            return Math.PI * r * r;
        }
    }
}
