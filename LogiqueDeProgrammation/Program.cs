﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogiqueDeProgrammation
{
    class Program
    {
        static void Main(string[] args)
        {
            bool finProgramme = false;
            while (!finProgramme)
            {
                Console.WriteLine("Choisissez un programme parmi les suivants :");
                Console.WriteLine("1 : SousP");
                Console.WriteLine("2 : Tableaux");
                int choix = Fonctions.ReponseEntiere("3 : Puissance 4");
                switch (choix)
                {
                    case 1:
                        finProgramme = !SousP();
                        break;
                    case 2:
                        finProgramme = !ExercicesTableaux.Program();
                        break;
                    case 3:
                        finProgramme = !Puissance4.Program();
                        break;
                    default:
                        Console.WriteLine("Mauvais choix.");
                        break;
                }
            }
        }
        static bool SousP()
        {
            bool finProgramme = false;
            while (!finProgramme)
            {
                Console.WriteLine("Choisissez un programme parmi les suivants :");
                Console.WriteLine("1 : Affectations de variables (Slide 20/21)");
                Console.WriteLine("2 : Opérateurs arithmériques (Slide 27/28)");
                Console.WriteLine("3 : Carré");
                Console.WriteLine("4 : Triangle");
                Console.WriteLine("5 : Octogone");
                Console.WriteLine("6 : Plus petit, plus grand");
                Console.WriteLine("7 : Périmètre et aire d'un cercle");
                Console.WriteLine("9 : Opérateurs logiques (Slide 34/39");
                int choix = Fonctions.ReponseEntiere("8 : Manipulation de strings");
                switch (choix)
                {
                    case 1:
                        finProgramme = !AffectationsSlide20.Program();
                        break;
                    case 2:
                        finProgramme = !OpArithmetiqueSlide27.Program();
                        break;
                    case 9:
                        finProgramme = !OpLogiqueSlide34.Menu();
                        break;
                    case 3:
                        finProgramme = !Carre.Menu();
                        break;
                    case 4:
                        finProgramme = !Triangle.Menu();
                        break;
                    case 5:
                        finProgramme = !Octogone.Menu();
                        break;
                    case 6:
                        finProgramme = !PlusPetitPlusGrand.Jeu();
                        break;
                    case 7:
                        finProgramme = !PerimetreAireCercle.Menu();
                        break;
                    case 8:
                        finProgramme = !StringManipulation.Program();
                        break;
                    default:
                        Console.WriteLine("Mauvais choix.");
                        break;
                }
            }
            return true;
        }
        static bool Tableaux()
        {
            bool finProgramme = false;
            while (!finProgramme)
            {
                Console.WriteLine("Choisissez un programme parmi les suivants :");
                Console.WriteLine("1 : Affectations de variables (Slide 20/21)");
                Console.WriteLine("2 : Opérateurs arithmériques (Slide 27/28)");
                Console.WriteLine("3 : Carré");
                Console.WriteLine("4 : Triangle");
                Console.WriteLine("5 : Octogone");
                Console.WriteLine("6 : Plus petit, plus grand");
                Console.WriteLine("7 : Périmètre et aire d'un cercle");
                Console.WriteLine("9 : Opérateurs logiques (Slide 34/39");
                int choix = Fonctions.ReponseEntiere("8 : Manipulation de strings");
                switch (choix)
                {
                    case 1:
                        finProgramme = !AffectationsSlide20.Program();
                        break;
                    case 2:
                        finProgramme = !OpArithmetiqueSlide27.Program();
                        break;
                    case 9:
                        finProgramme = !OpLogiqueSlide34.Menu();
                        break;
                    case 3:
                        finProgramme = !Carre.Menu();
                        break;
                    case 4:
                        finProgramme = !Triangle.Menu();
                        break;
                    case 5:
                        finProgramme = !Octogone.Menu();
                        break;
                    case 6:
                        finProgramme = !PlusPetitPlusGrand.Jeu();
                        break;
                    case 7:
                        finProgramme = !PerimetreAireCercle.Menu();
                        break;
                    case 8:
                        finProgramme = !StringManipulation.Program();
                        break;
                    default:
                        Console.WriteLine("Mauvais choix.");
                        break;
                }
            }
            return true;
        }
    }
}
