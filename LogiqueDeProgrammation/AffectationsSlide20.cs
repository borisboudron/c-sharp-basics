﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogiqueDeProgrammation
{
    static class AffectationsSlide20
    {
        public static bool Program()
        {
            int a = Fonctions.ReponseEntiere("Donnez 1 valeur.");
            int b = Fonctions.ReponseEntiere("Donnez 1 autre valeur.");
            Console.WriteLine("Vous avez introduit les valeurs {0} et {1}", a, b);
            Console.WriteLine("a <- b");
            a = b;
            Console.WriteLine("b <- a");
            b = a;
            Console.WriteLine("Les nouvelles valeurs sont {0} et {1}", a, b);
            Console.WriteLine("Si vous vouliez inverser les valeurs des 2 variables, une 1ère astuce était de se servir d'une variable intermédiaire :");
            Console.WriteLine("temp <- b");
            Console.WriteLine("b <- a");
            Console.WriteLine("a <- temp");
            return Fonctions.ReponseBool("Voulez-vous exécuter un autre programme ? (o ou n)");
        }
    }
}
