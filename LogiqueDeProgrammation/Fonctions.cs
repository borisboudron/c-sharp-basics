﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogiqueDeProgrammation
{
    static class Fonctions
    {
        public static string RepeteCar(char car, int n)
        {
            string chaine = "";
            for (int i = 0; i < n; i++)
                chaine += car;
            return chaine;
        }
        public static int ReponseEntiere(string question)
        {
            Console.WriteLine(question);
            int reponse = -1;
            bool reponseValable = false;
            while (!reponseValable)
            {
                if (int.TryParse(Console.ReadLine(), out reponse))
                    reponseValable = true;
                else
                    Console.WriteLine("Ce n'est pas une réponse entière. Veuillez recommencer.");
            }
            return reponse;
        }
        public static bool ReponseBool(string question)
        {
            Console.WriteLine(question);
            bool reponse = false;
            bool reponseValable = false;
            while (!reponseValable)
            {
                switch (Console.ReadLine())
                {
                    case "o":
                        reponse = true;
                        reponseValable = true;
                        break;
                    case "n":
                        reponse = false;
                        reponseValable = true;
                        break;
                    default:
                        Console.WriteLine("Ce n'est pas une réponse valable. Veuillez recommencer.");
                        break;
                }
            }
            return reponse;
        }
    }
}
