﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogiqueDeProgrammation
{
    static class Carre
    {
        public static bool Menu()
        {
            int longueur = Fonctions.ReponseEntiere("Quelle est la longueur du carré souhaité ?");
            bool plein = Fonctions.ReponseBool("Voulez-vous un carré plein (o) ou creux (n) ?");
            DessineCarre(longueur, plein);
            return Fonctions.ReponseBool("Voulez-vous exécuter un autre programme ? (o ou n)");
        }
        public static int DessineCarre(int l, bool plein)
        {
            // Construction
            string chaineExtremite = Fonctions.RepeteCar('*', l);
            if (plein)
            // Carré plein
            {
                // Affichage pour chaque ligne
                for (int i = 0; i < l; i++)
                    Console.WriteLine(chaineExtremite);
            }
            else
            // Carré creux
            {
                string chaineMilieu = '*' + Fonctions.RepeteCar(' ', l - 2) + '*';
                // Affichage
                // 1ère ligne
                Console.WriteLine(chaineExtremite);
                // Lignes intérieures
                for (int i = 1; i < l - 1; i++)
                    Console.WriteLine(chaineMilieu);
                //Dernière ligne
                Console.WriteLine(chaineExtremite);
            }
            return 0;
        }
    }
}
